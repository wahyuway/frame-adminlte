'use strict';


exports.register = function (server, options, next) {

    server.route({
        method: 'GET',
        path: '/{path*}',
        handler: {
          directory: {
              path: './node_modules/admin-lte',
              index: true
          }
        }
    });


    next();
};


exports.register.attributes = {
    name: 'home',
    dependencies: 'visionary'
};
